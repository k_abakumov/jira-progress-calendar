package org.aksoftware.jira.plugins.api;

public interface MyPluginComponent
{
    String getName();
}