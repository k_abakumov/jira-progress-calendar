package ut.org.aksoftware.jira.plugins;

import org.junit.Test;
import org.aksoftware.jira.plugins.api.MyPluginComponent;
import org.aksoftware.jira.plugins.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}